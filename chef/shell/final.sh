#!/bin/bash

VDD_IP=$(echo "$1")


if which npm >/dev/null; then	
  echo "Node.JS already installed.";    
else
  echo >&2 "I require Node.JS and it's not installed. Installing it."
  # Install node.js
  (cd /tmp && wget http://nodejs.org/dist/v0.12.4/node-v0.12.4.tar.gz)
  (cd /tmp && tar -xf node-v0.12.4.tar.gz)
  (cd /tmp/node-v0.12.4 && ./configure && make && sudo make install)
fi

if which compass >/dev/null; then	
  echo "Compass already installed.";    
else
  echo >&2 "I require Compass and it's not installed. Installing it."
  # Install compass
  sudo gem install compass
fi




echo "============================================================="
echo "Install finished! Visit http://$VDD_IP in your browser." 
echo "============================================================="
